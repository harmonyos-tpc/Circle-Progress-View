# Circle-Progress-View

## Introduction
   An animated circle view. Can be used in 'value mode' or 'spinning mode'. Nice transitions between spinning and value.
   Can be used as a loading indicator and to show progress or values in a circular manner.
   In seek mode, it can also be used to set a value.

## Usage Instruction

Supported Features with usage Examples
======================================

### Circle-Progress-View for showing progress in value mode :
Circle-Progress-View can be used to show progress in value mode
   ```xml
	<at.grabner.circleprogress.CircleProgressView
        ohos:id="$+id:circleView"
        ohos:width="277vp"
        ohos:height="277vp"
        CircleProgressView:cpv_autoTextSize="true"
        CircleProgressView:cpv_barColor="#2196F3"
        CircleProgressView:cpv_barWidth="35vp"
        CircleProgressView:cpv_innerContourSize="0vp"
        CircleProgressView:cpv_maxValue="100"
        CircleProgressView:cpv_outerContourSize="0vp"
        CircleProgressView:cpv_rimColor="#BBDEFB"
        CircleProgressView:cpv_rimWidth="35vp"
        CircleProgressView:cpv_seekMode="true"
        CircleProgressView:cpv_showUnit="true"
        CircleProgressView:cpv_spinColor="#2196F3"
        CircleProgressView:cpv_textColor="#2196F3"
        CircleProgressView:cpv_textScale="1"
        CircleProgressView:cpv_unit="%"
        CircleProgressView:cpv_unitColor="#BBDEFB"
        CircleProgressView:cpv_unitPosition="3"
        CircleProgressView:cpv_unitScale="1"
        CircleProgressView:cpv_value="10"/>

```


### Circle-Progress-View for showing progress in spinning mode
 Circle-Progress-View can be used to show progress in spinning mode and also used for indicator loading
   ```xml
	<at.grabner.circleprogress.CircleProgressView
            ohos:id="$+id:circleView"
            ohos:width="277vp"
            ohos:height="277vp"
            CircleProgressView:cpv_autoTextSize="true"
            CircleProgressView:cpv_barColor="#2196F3"
            CircleProgressView:cpv_barWidth="35vp"
            CircleProgressView:cpv_innerContourSize="0vp"
            CircleProgressView:cpv_maxValue="100"
            CircleProgressView:cpv_outerContourSize="0vp"
            CircleProgressView:cpv_rimColor="#BBDEFB"
            CircleProgressView:cpv_rimWidth="35vp"
            CircleProgressView:cpv_seekMode="true"
            CircleProgressView:cpv_showUnit="true"
            CircleProgressView:cpv_spinColor="#2196F3"
            CircleProgressView:cpv_textColor="#2196F3"
            CircleProgressView:cpv_textScale="1"
            CircleProgressView:cpv_unit="%"
            CircleProgressView:cpv_unitColor="#BBDEFB"
            CircleProgressView:cpv_unitPosition="3"
            CircleProgressView:cpv_unitScale="1"
            CircleProgressView:cpv_value="10"/>

## attr:
cpv_direction -
    CW - 0
    CCW - 1

cpv_barStartEndLine -
    None - 0
    Start - 1
    End - 2
    Both - 3

cpv_unitPosition -
    top - 0
    bottom - 1
    left_top - 2
    right-top - 3
    left-bottom - 4
    right-bottom - 5
```
## Installation instruction
Method 1:
Generate the .har package through the library and add the .har package to the libs folder.
Add the following code to the entry gradle:
    implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])

Method 2:
Add following dependencies in entry build.gradle:**

    entry build.gradle:
    dependencies {
            implementation project(':CircleProgressView')
    }

Method 3. For using CircleProgressView library from a remote repository in separate application, add the below dependency in entry/build.gradle file.

    dependencies {
            implementation 'io.openharmony.tpc.thirdlib:Circle-Progress-View:1.0.1'
    }