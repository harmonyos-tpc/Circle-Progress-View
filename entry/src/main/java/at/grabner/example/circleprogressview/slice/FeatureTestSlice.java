/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package at.grabner.example.circleprogressview.slice;

import at.grabner.circleprogress.*;
import at.grabner.circleprogress.curvetypes.AccelerateCurveType;
import at.grabner.circleprogress.curvetypes.AccelerateDecelerateCurveType;
import at.grabner.circleprogress.curvetypes.BounceCurveType;
import at.grabner.circleprogress.curvetypes.DecelerateCurveType;
import at.grabner.example.circleprogressview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;

public class FeatureTestSlice extends AbilitySlice {
    private CircleProgressView mCircleView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_feature);

        mCircleView = (CircleProgressView) findComponentById(ResourceTable.Id_circleView);
        mCircleView.setShowTextWhileSpinning(true);

        mCircleView.setTextSize(40);
        mCircleView.setUnitPosition(UnitPosition.RIGHT_TOP);
        mCircleView.setUnitSize(40);
        mCircleView.setText(Constant.Indicator_LOADING);

        mCircleView.setValueInterpolator(new AccelerateDecelerateCurveType());


        Button size = (Button) findComponentById(ResourceTable.Id_size);
        size.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setTextSize(100);
                mCircleView.setUnitSize(100);
                mCircleView.setUnitPosition(UnitPosition.RIGHT_TOP);
            }
        });

        Button innerouter = (Button) findComponentById(ResourceTable.Id_innerouter);
        innerouter.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setBarWidth(40);
                mCircleView.setRimWidth(40);
                mCircleView.setInnerContourSize(3.0f);
                mCircleView.setInnerContourColor(Color.RED.getValue());
                mCircleView.setShowBlock(true);
                mCircleView.setOuterContourSize(3.0f);
                mCircleView.setOuterContourColor(Color.RED.getValue());
            }
        });

        Button delaymillisec = (Button) findComponentById(ResourceTable.Id_delaymillisec);
        delaymillisec.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setBarColor(Color.RED.getValue());
                mCircleView.spin();
                mCircleView.setDelayMillis(0);
            }
        });

        Button barstartendline = (Button) findComponentById(ResourceTable.Id_barstartendline);
        barstartendline.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setBarStartEndLine(4, BarStartEndLine.BOTH,Color.RED.getValue(),20.0F);
            }
        });

        Button animatedvalue = (Button) findComponentById(ResourceTable.Id_animatedvalue);
        animatedvalue.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setAutoTextSize(true);
                mCircleView.setBarColor(Color.GREEN.getValue());
                mCircleView.setValueAnimated(60, 80, 7000);
            }
        });

        Button typeinterface = (Button) findComponentById(ResourceTable.Id_typeinterface);
        typeinterface.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setAutoTextSize(true);
                mCircleView.setTextTypeface(Font.SERIF);
                mCircleView.setUnitTextTypeface(Font.SERIF);
            }
        });

        Button spinnerstrokecap = (Button) findComponentById(ResourceTable.Id_spinnerstrokecap);
        spinnerstrokecap.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.spin();
                mCircleView.setSpinnerStrokeCap(Paint.StrokeCap.ROUND_CAP);
            }
        });

        Button direction = (Button) findComponentById(ResourceTable.Id_direction);
        direction.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
               mCircleView.setDirection(Direction.CCW);
            }
        });

        Button fillcolor = (Button) findComponentById(ResourceTable.Id_fillcolor);
        fillcolor.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setFillCircleColor(Color.GRAY.getValue());
            }
        });

        Button progresschanged = (Button) findComponentById(ResourceTable.Id_progresschanged);
        progresschanged.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
                    @Override
                    public void onProgressChanged(float v) {
                        ToastDialog toastDialog = new ToastDialog(getContext());
                        toastDialog.setText("onprogresschanged " + v).setDuration(1000).show();
                    }
                });
            }
        });

        Button seek = (Button) findComponentById(ResourceTable.Id_seek);
        seek.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setSeekModeEnabled(false);
            }
        });

        Button spinspeed = (Button) findComponentById(ResourceTable.Id_spinspeed);
        spinspeed.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setSpinSpeed(5);
                mCircleView.spin();
            }
        });

        Button curvetype = (Button) findComponentById(ResourceTable.Id_curvetype);
        curvetype.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setValueInterpolator(new AccelerateCurveType(1.2f));
                mCircleView.setValueAnimated(60, 80, 7000);
            }
        });
    }
}
