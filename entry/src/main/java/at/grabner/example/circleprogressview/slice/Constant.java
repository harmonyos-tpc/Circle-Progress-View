/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package at.grabner.example.circleprogressview.slice;

public class Constant {
    public static String POSITION_LEFT_TOP = "Left Top";
    public static String POSITION_LEFT_BOTTOM = "Left Bottom";
    public static String POSITION_RIGHT_TOP = "Right Top";
    public static String POSITION_RIGHT_BOTTOM = "Right Bottom";
    public static String POSITION_TOP = "Top";
    public static String POSITION_BOTTOM = "Bottom";
    public static String Indicator_LOADING = "Loading";
}