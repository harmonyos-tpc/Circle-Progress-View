/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package at.grabner.example.circleprogressview.slice;


import at.grabner.circleprogress.*;
import at.grabner.example.circleprogressview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;

public class MainAbilitySlice extends AbilitySlice {
  private CircleProgressView mCircleView;
    Switch mSwitchSpin;
    Switch mSwitchShowUnit;
    Slider mSeekBar;
    Slider mSeekBarSpinnerLength;
    Boolean mShowUnit = true;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mCircleView =(CircleProgressView)findComponentById(ResourceTable.Id_circleView);
        mCircleView.setShowTextWhileSpinning(true);

        mCircleView.setTextSize(40);
        mCircleView.setUnitPosition(UnitPosition.RIGHT_TOP);
        mCircleView.setUnitSize(40);
        mCircleView.setAutoTextSize(true);
        mCircleView.setText(Constant.Indicator_LOADING);
        Button lefttop = (Button) findComponentById(ResourceTable.Id_lefttop);
        lefttop.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setUnitPosition(UnitPosition.LEFT_TOP);
            }
        });

        Button leftbottom = (Button) findComponentById(ResourceTable.Id_leftbottom);
        leftbottom.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setUnitPosition(UnitPosition.LEFT_BOTTOM);
            }
        });

        Button righttop = (Button) findComponentById(ResourceTable.Id_righttop);
        righttop.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setUnitPosition(UnitPosition.RIGHT_TOP);
            }
        });

        Button rightbottom = (Button) findComponentById(ResourceTable.Id_rightbottom);
        rightbottom.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setUnitPosition(UnitPosition.RIGHT_BOTTOM);
            }
        });

        Button bottom = (Button) findComponentById(ResourceTable.Id_bottom);
        bottom.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setUnitPosition(UnitPosition.BOTTOM);
            }
        });

        Button top = (Button) findComponentById(ResourceTable.Id_top);
        top.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCircleView.setUnitPosition(UnitPosition.TOP);
            }
        });

        Button feature = (Button) findComponentById(ResourceTable.Id_feature);
        feature.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                present(new FeatureTestSlice(),intent);
            }
        });

        mCircleView.setOnAnimationStateChangedListener(new AnimationStateChangedListener() {
            @Override
            public void onAnimationStateChanged(AnimationState _animationState) {
                switch (_animationState) {
                    case IDLE:
                    case ANIMATING:
                    case START_ANIMATING_AFTER_SPINNING:
                        mCircleView.setTextMode(TextMode.PERCENT); // show percent if not spinning
                        mCircleView.setUnitVisible(true);
                        break;
                    case SPINNING:
                        mCircleView.setTextMode(TextMode.TEXT); // show text while spinning
                        mCircleView.setUnitVisible(false);
                    case END_SPINNING:
                        break;
                    case END_SPINNING_START_ANIMATING:
                        break;
                }
            }
        });

        mSwitchSpin = (Switch)findComponentById(ResourceTable.Id_switch1);
        mSwitchSpin.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                if (isChecked) {
                    mCircleView.spin();
                } else {
                    mCircleView.stopSpinning();
                }
            }
        });

        mSwitchShowUnit = (Switch)findComponentById(ResourceTable.Id_switch2);
        mSwitchShowUnit.setChecked(mShowUnit);
        mSwitchShowUnit.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                mCircleView.setUnitVisible(isChecked);
                mShowUnit = isChecked;
            }
        });

        mSeekBar = (Slider)findComponentById(ResourceTable.Id_slider1);
        mSeekBar.setMaxValue(100);
        mSeekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {

            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                mCircleView.setValueAnimated(mSeekBar.getProgress(),1500);
                mSwitchSpin.setChecked(false);
            }
        });

        mSeekBarSpinnerLength = (Slider)findComponentById(ResourceTable.Id_slider2);
        mSeekBarSpinnerLength.setMaxValue(360);
        mSeekBarSpinnerLength.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {

            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                mCircleView.setSpinningBarLength(mSeekBarSpinnerLength.getProgress());

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
