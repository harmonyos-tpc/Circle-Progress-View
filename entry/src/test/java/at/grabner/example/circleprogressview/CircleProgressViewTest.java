/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package at.grabner.example.circleprogressview;

import at.grabner.circleprogress.CircleProgressView;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircleProgressViewTest {

    Context context;
    private static int MAX_VALUE = 100;
    private static String UNIT = "%";
    private static int UNIT_SIZE = 60;
    private static int TEXT_SIZE = 60;
    private static int UNIT_SCALE = 60;

    @Test
    public void shouldSetMaxValue() {
        CircleProgressView circleProgressView = new CircleProgressView(context);
        circleProgressView.setMaxValue(MAX_VALUE);
        assertEquals(100, circleProgressView.getMaxValue(), 0);
    }

    @Test
    public void shouldsetUnit() {
        CircleProgressView circleProgressView = new CircleProgressView(context);
        circleProgressView.setUnit(UNIT);
        assertEquals("%",circleProgressView.getUnit());
    }

    @Test
    public void shouldsetunitSize() {
        CircleProgressView circleProgressView = new CircleProgressView(context);
        circleProgressView.setUnitSize(UNIT_SIZE);
        assertEquals(60,circleProgressView.getUnitSize());
    }

    @Test
    public void shouldsetTextSize() {
        CircleProgressView circleProgressView = new CircleProgressView(context);
        circleProgressView.setUnitSize(TEXT_SIZE);
        assertEquals(60,circleProgressView.getTextSize());
    }

    @Test
    public void shouldsetUnitScale() {
        CircleProgressView circleProgressView = new CircleProgressView(context);
        circleProgressView.setUnitScale(UNIT_SCALE);
        assertEquals(1, circleProgressView.getUnitScale(), 0);
    }
}
