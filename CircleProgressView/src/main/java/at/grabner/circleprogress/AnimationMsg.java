package at.grabner.circleprogress;

public enum AnimationMsg {

    START_SPINNING,
    STOP_SPINNING,
    SET_VALUE,
    SET_VALUE_ANIMATED,
    TICK
}
