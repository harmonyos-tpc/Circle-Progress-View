package at.grabner.circleprogress;

import ohos.agp.render.Paint;

public enum StrokeCap {

    /**
     * The stroke ends with the path, and does not project beyond it.
     */
    BUTT(Paint.StrokeCap.BUTT_CAP),
    /**
     * The stroke projects out as a semicircle, with the center at the
     * end of the path.
     */
    ROUND( Paint.StrokeCap.ROUND_CAP),
    /**
     * The stroke projects out as a square, with the center at the end
     * of the path.
     */
    SQUARE(Paint.StrokeCap.SQUARE_CAP);

    final Paint.StrokeCap paintCap;

    StrokeCap(Paint.StrokeCap paintCap) {
        this.paintCap = paintCap;
    }
}
