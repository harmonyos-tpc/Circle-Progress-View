/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.grabner.circleprogress.curvetypes;

import ohos.agp.animation.Animator;

/**
 * Backport
 */
public class AccelerateCurveType extends Animator.CurveType implements TimeCurveType {
    private final float mFactor;
    private final double mDoubleFactor;


    public AccelerateCurveType(float factor) {
        mFactor = factor;
        mDoubleFactor = 2 * mFactor;
    }

    public float getCurveType(float input) {
        if (mFactor == 1.0f) {
            return input * input;
        } else {
            return (float) Math.pow(input, mDoubleFactor);
        }
    }
}
