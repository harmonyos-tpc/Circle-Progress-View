package at.grabner.circleprogress.curvetypes;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

public class BounceCurveType implements TimeCurveType{

    public BounceCurveType() {
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public BounceCurveType(Context context, AttrSet attrs) {
    }

    private static float bounce(float t) {
        return t * t * 8.0f;
    }

    @Override
    public float getCurveType(float t) {
        t *= 1.1226f;
        if (t < 0.3535f) return bounce(t);
        else if (t < 0.7408f) return bounce(t - 0.54719f) + 0.7f;
        else if (t < 0.9644f) return bounce(t - 0.8526f) + 0.9f;
        else return bounce(t - 1.0435f) + 0.95f;
    }
}
